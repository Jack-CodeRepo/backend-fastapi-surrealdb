from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
from app.routers import order_routers
from pathlib import Path

app = FastAPI(
    title="FastApi backend with SurrealDB",
    docs_url="/api-docs",
    redoc_url=None
)


allowed_origins = [
    "http://local_frontend_url:its_port",
    "http://any_other_url.europe",
    "https://yau-yetanotherurl.org"
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=allowed_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


app.include_router(order_routers.router)


@app.get("/")
async def root():
    return {"message": "FastApi is launched"}

@app.get("/favicon.ico")
async def root():
    cwd_path =  Path.cwd()
    return FileResponse(f"{cwd_path}/app/favicon.ico")