from abc import ABC, abstractmethod


class IProductRepository(ABC):

    @abstractmethod
    def get_product_by_id(self, p_productID: str):
        pass

