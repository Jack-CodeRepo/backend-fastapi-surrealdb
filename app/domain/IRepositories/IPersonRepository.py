from abc import ABC, abstractmethod


class IPersonRepository(ABC):

    @abstractmethod
    def get_person_by_id(self, p_personID: str):
        pass

