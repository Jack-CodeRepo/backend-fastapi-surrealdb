from abc import ABC, abstractmethod

class IOrderRepository(ABC):

    @abstractmethod
    def get_all_order_from_person_id(self, p_personID: str):
        pass

    @abstractmethod
    def get_all_order_made_for_product(self, p_productID: str):
        pass