from typing import Optional
from pydantic import BaseModel

from app.domain.models.address_model import Address




class Order(BaseModel):
    id: str
    currency: str
    discount: Optional[float]
    order_date: str
    order_status: Optional[str]               # ['pending','processing','delivered','shipped',NONE]
    payment_method: str             # ['PayPal','debit card','credit card']
    price: float = 0.0
    product_name: str
    quantity: int = 0
    shipping_address: Address
    person_id: str             # Person / in
    product_id: str              # Product / out
