from typing import List, Optional
from pydantic import BaseModel


class Address(BaseModel):
    address_line_1: str
    address_line_2: Optional[str]
    city: str
    coordinates: List[float]
    country: str
    post_code: str
