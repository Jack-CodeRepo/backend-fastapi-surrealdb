from typing import Optional
from pydantic import BaseModel

class Product(BaseModel):
    id: str
    category: str
    currency: str
    description: str
    discount: Optional[float]
    image_url: str
    name: str
    price: float
    quantity: int
