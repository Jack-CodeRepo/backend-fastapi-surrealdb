from typing import Optional

from pydantic import BaseModel

from app.domain.models.address_model import Address

"""
    This model is valid for the following tables
        - person
        - artist

"""

class Person(BaseModel):
    id: str
    address: Address
    company_name: Optional[str]
    email: str
    first_name: str
    last_name: str
    name: str
    phone: str