from app.domain.builders.output_models.PersonOM import PersonDTO
from app.domain.models.person_model import Person


def convert_Person_to_PersonDTO(p_person: Person) -> PersonDTO:
    return PersonDTO(**p_person.model_dump())