from typing import Optional, List

from pydantic import BaseModel


class AddressDTO(BaseModel):
    address_line_1: str
    address_line_2: Optional[str]
    city: str
    coordinates: List[float]
    country: str
    post_code: str