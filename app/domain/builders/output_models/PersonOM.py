from typing import Optional

from pydantic import BaseModel

from app.domain.builders.output_models.AddressOM import AddressDTO


class PersonDTO(BaseModel):
    address: AddressDTO
    company_name: Optional[str]
    email: str
    first_name: str
    last_name: str
    name: str
    phone: str