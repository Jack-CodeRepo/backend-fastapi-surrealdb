from typing import Optional

from pydantic import BaseModel

from app.domain.builders.output_models.PersonOM import PersonDTO
from app.domain.builders.output_models.ProductOM import ProductDTO
from app.domain.builders.output_models.AddressOM import AddressDTO


class OrderDTO(BaseModel):
    person: Optional[PersonDTO] = None
    product: Optional[ProductDTO] = None
    currency: str
    discount: Optional[float]
    order_date: str
    order_status: Optional[str]
    payment_method: str
    price: float
    product_name: str
    quantity: int
    shipping_address: AddressDTO


