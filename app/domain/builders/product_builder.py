from app.domain.builders.output_models.ProductOM import ProductDTO
from app.domain.models.product_model import Product


def convert_Product_to_ProductDTOO(p_person: Product) -> ProductDTO:
    return ProductDTO(**p_person.model_dump())