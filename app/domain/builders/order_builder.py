
from app.domain.builders.output_models.OrderOM import OrderDTO
from app.domain.builders.output_models.PersonOM import PersonDTO
from app.domain.builders.output_models.ProductOM import ProductDTO
from app.domain.models.order_model import Order


def convert_to_OrderDTO(order: Order, product: ProductDTO, person: PersonDTO) -> OrderDTO:
        out_order = OrderDTO(**order.model_dump())
        out_order.product = product
        out_order.person = person

        return out_order