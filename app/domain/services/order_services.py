from typing_extensions import List

from app.domain.IRepositories import IOrderRepository
from app.database.order_repo import OrderRepository
from app.database.person_repo import PersonRepository
from app.database.product_repo import ProductRepository

from app.domain.builders.output_models.OrderOM import OrderDTO
from app.domain.builders.output_models.PersonOM import PersonDTO
from app.domain.builders.output_models.ProductOM import ProductDTO

from app.domain.builders.order_builder import convert_to_OrderDTO
from app.domain.builders.product_builder import convert_Product_to_ProductDTOO
from app.domain.builders.person_builder import convert_Person_to_PersonDTO





class OrderService:

    def __init__(self):
        self.order_repository: IOrderRepository = OrderRepository()
        self.product_repository = ProductRepository()
        self.person_repository = PersonRepository()


    async def get_all_order_from_person_id(self,p_personID: str) -> List[OrderDTO]:
        order_list = await self.order_repository.get_all_order_from_person_id(p_personID)


        r_order_list = []
        for order in order_list:
            product: ProductDTO = convert_Product_to_ProductDTOO(await self.product_repository.get_product_by_id(order.product_id))
            person: PersonDTO = convert_Person_to_PersonDTO(await self.person_repository.get_person_by_id(order.person_id))

            r_order_list.append(convert_to_OrderDTO(order, product, person))

        return r_order_list


    async def get_all_order_made_for_product(self,p_productID: str) -> List[OrderDTO]:
        order_list = await self.order_repository.get_all_order_made_for_product(p_productID)

        r_order_list = []
        for order in order_list:
            product: ProductDTO = convert_Product_to_ProductDTOO(await self.product_repository.get_product_by_id(order.product_id))
            person: PersonDTO = convert_Person_to_PersonDTO(await self.person_repository.get_person_by_id(order.person_id))

            r_order_list.append(convert_to_OrderDTO(order, product, person))

        return r_order_list
