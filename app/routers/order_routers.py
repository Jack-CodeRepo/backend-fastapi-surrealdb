from fastapi import APIRouter

from app.database.order_repo import OrderRepository
from app.domain.services.order_services import OrderService


router = APIRouter(
    prefix="/orders",
    tags=["orders"]
)


order_repo = OrderRepository()
order_service = OrderService()


@router.get("/from_person_id/{p_personID}")
async def get_all_order_from_person_id(p_personID: str):
    return await order_service.get_all_order_from_person_id(p_personID)


@router.get("/for_product_id/{p_productID}")
async def get_all_order_made_for_product_id(p_productID: str):
    return await order_service.get_all_order_made_for_product(p_productID)

