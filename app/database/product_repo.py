from app.domain.IRepositories.IProductRepository import IProductRepository
from app.domain.models.product_model import Product
from app.database.surrealdb.database import get_surreal_client
from app.utils import get_result_from_query


class ProductRepository(IProductRepository):

    async def get_product_by_id(self, p_productID: str) -> Product:
        query = f"select * from only {p_productID}"
        result = get_result_from_query(await get_surreal_client().query(query))
        return Product(**result)
