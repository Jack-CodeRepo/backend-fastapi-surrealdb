from typing import List

from app.database.surrealdb.database import get_surreal_client
from app.domain.models.order_model import Order
from app.domain.IRepositories.IOrderRepository import IOrderRepository
from app.utils import get_result_from_query


class OrderRepository(IOrderRepository):


    async def get_all_order_from_person_id(self, p_personID: str) -> List[Order]:
        query = f"select ->order.* from {p_personID};"
        result = get_result_from_query(await get_surreal_client().query(query))[0]["->order"]

        order_list = []
        for r in result:
            tmp_order = dict(**r)
            tmp_order.update({"person_id": r.pop("in"), "product_id":r.pop("out")})
            o = Order(**tmp_order)
            order_list.append(o)
        return order_list



    async def get_all_order_made_for_product(self, p_productID: str) -> List[Order]:
        query = f"select <-order.* from {p_productID};"
        result = get_result_from_query(await get_surreal_client().query(query))[0]["<-order"]

        order_list = []
        for r in result:
            tmp_order = dict(**r)
            tmp_order.update({"person_id": r.pop("in"), "product_id":r.pop("out")})
            o = Order(**tmp_order)
            order_list.append(o)
        return order_list
