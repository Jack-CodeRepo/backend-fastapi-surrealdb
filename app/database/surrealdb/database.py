from surrealdb import SurrealHTTP


URL = "http://localhost:8010"


def get_surreal_client():
    client = SurrealHTTP(url=URL,
                     namespace="sns",
                     database="sdb",
                     username="root01",
                     password="root01")
    return client



def execute_query(p_query:str):
    return get_surreal_client().query(p_query)