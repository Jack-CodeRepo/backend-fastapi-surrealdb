from app.database.surrealdb.database import get_surreal_client
from app.domain.models.person_model import Person
from app.domain.IRepositories.IPersonRepository import IPersonRepository
from app.utils import get_result_from_query


class PersonRepository(IPersonRepository):

    async def get_person_by_id(self, p_personID: str) -> Person:
        query = f"select * from only {p_personID}"
        result = get_result_from_query(await get_surreal_client().query(query))
        return Person(**result)