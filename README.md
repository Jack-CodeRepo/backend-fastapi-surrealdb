# Backend FastApi Surrealdb


## Introduction
Quick implementation of backend with [FastApi](https://github.com/tiangolo/fastapi) and [SurrealDB](https://github.com/surrealdb/surrealdb)

A light database is provided at root of project. Its name is "demo_data.surql"

Please read [official documentation](https://surrealdb.com/docs/surrealdb/installation/) to install SurrealDB

## Commands

### Step 01
Launch Surrealdb binary
```
/path/of/binary/surreal start --auth --user root01 --pass root01 --bind 0.0.0.0:8010 --log trace memory
``` 

### Step 02
Import data (surrealdb must be running)
```
surreal import --conn http://localhost:8010 --user root01 --pass root01 --ns sns --db sdb demo_data.surql    
```

### Step 03 (optionnal)
Export data (surrealdb must be running)
Only if you wish to save your modifications
```
surreal export --conn http://localhost:8010 --user root01 --pass root01 --ns sns --db sdb demo_data.surql     
```


